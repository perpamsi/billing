<?php
	if($erno) die();
	$kar_id = _USER;
	$byr_no	= _TOKN;
	$kopel	= $_SESSION['kp_ket'];
	
	/** koneksi ke database */
	$db		= false;
	try {
		$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		unset($mess);
	}
	catch (PDOException $err){
		$mess = $err->getTrace();
		errorLog::errorDB(array($mess[0]['args'][0]));
		$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
		$klas = "error";
	}
	switch($proses){
		case "setResi":
			$db->beginTransaction();
			if($db){
				try {
					$que	= "INSERT INTO system_parameter(sys_param,sys_value,sys_value1,sys_value2) VALUES('RESI','$kar_id','$noresi','-') ON DUPLICATE KEY UPDATE sys_value1='$noresi'";
					$st 	= $db->exec($que);
					if($st>0){
						$db->commit();
						errorLog::logDB(array($que));
						$mess = "Set resi: $noresi telah dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
					}
					else{
						$db->rollBack();
						$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
					}
				}
				catch (PDOException $err){
					$db->rollBack();
					$mess = $err->getTrace();
					errorLog::errorDB(array($mess[0]['args'][0]));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
				}
			}
			break;
		case "bayar":
			try{
				$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
				$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
				$cetak 		= true;
			}
			catch (Exception $e){
				$mess		= "Perangkat pencetak belum tersedia";
				errorLog::errorDB(array($mess));
				$mess		= $e->getMessage();
				$klas		= "error";
				$cetak 		= false;
			}
			
			if($cetak){
				$db->beginTransaction();
				$j 		= 0;
				$l		= 0;
				$erno	= false;
				if($kembalian>=0){				
					if(!is_array($pel_no)){
						$tmp_no 	= $pel_no;
						$tmp_nama 	= $pel_nama;
						$tmp_alamat	= $pel_alamat;
						unset($pel_no);
						unset($pel_nama);
						unset($pel_alamat);
						for($i=0;$i<=count($pilih);$i++){
							$pel_no[$i] 	= $tmp_no;
							$pel_nama[$i] 	= $tmp_nama;
							$pel_alamat[$i]	= $tmp_alamat;
						}
					}

					// line untuk ff continous paper
					$stringCetak  = chr(27).chr(67).chr(1);
					// enable paper out sensor
					$stringCetak .= chr(27).chr(57);
					// draft mode
					$stringCetak .= chr(27).chr(120).chr(48);
					// mode 10 cpi
					$stringCetak .= chr(27).chr(80);
					for($i=0;$i<=count($pilih);$i++){
						if($db && $pilih[$i]==1){
							// Pengambilan rincian tarif air
							$que0	= "SELECT a.* FROM tm_kode_tarif a WHERE ".substr($rek_nomor[$i],0,6).">=a.tar_bln_mulai AND ".substr($rek_nomor[$i],0,6)."<=a.tar_bln_akhir AND a.gol_kode='".$rek_gol[$i]."'";
							/** getParam 
								memindahkan semua nilai dalam array POST ke dalam
								variabel yang bersesuaian dengan masih kunci array
							*/
							$res0 	= mysql_query($que0,$link);
							$nilai	= mysql_fetch_array($res0);
							$konci	= array_keys($nilai);
							for($k=0;$k<count($konci);$k++){
								$$konci[$k]	= $nilai[$konci[$k]];
							}
							/* getParam **/
							$o_awalA	= " ";
							$o_akhirA	= " ";
							$o_hargaA	= " ";
							$o_uangairA	= " ";
							$o_awalB	= " ";
							$o_akhirB	= " ";
							$o_hargaB	= " ";
							$o_uangairB	= " ";
							$o_awalC	= " ";
							$o_akhirC	= " ";
							$o_hargaC	= " ";
							$o_uangairC	= " ";
							$pemakaian	= $rek_pakai[$i];
							if($pemakaian>$tar_sd2){
								$o_awalC	= $tar_sd2+1;
								$o_akhirC	= $pemakaian;
								$o_hargaC	= $tar_3;
								$o_uangairC	= ($pemakaian-$tar_sd2)*$tar_3;
								$pemakaian 	= $tar_sd2;
							}
							if($pemakaian>$tar_sd1){
								$o_awalB	= $tar_sd1+1;
								$o_akhirB	= $pemakaian;
								$o_hargaB	= $tar_2;
								$o_uangairB = ($pemakaian-$tar_sd1)*$tar_2;
								$pemakaian 	= $tar_sd1;
							}
							if($pemakaian>0){
								$o_awalA	= 0;
								$o_akhirA	= $pemakaian;
								$o_hargaA	= $tar_1;
								$o_uangairA = $pemakaian*$tar_1;
							}
						
							try {
								// line spacing x/n
								$stringCetak .= chr(27).chr(65).chr(28);
								$stringCetak .= chr(10);

								if($l>0){
									if($l%3==0){
										// line spacing x/n
										$stringCetak .= chr(27).chr(65).chr(23);
										$stringCetak .= chr(10);
									}
									else{
										// line spacing x/n
										$stringCetak .= chr(27).chr(65).chr(25);
										$stringCetak .= chr(10);
									}
								}

								// line spacing x/n
								$stringCetak .= chr(27).chr(65).chr(12);
								$stringCetak .= str_repeat(' ',10).printLeft($bulan[$rek_bln[$i]]." ".$rek_thn[$i],14).	"             ".printLeft($pel_no[$i],25).$bulan[$rek_bln[$i]]." ".$rek_thn[$i].chr(10);
								$stringCetak .= str_repeat(' ',10).printLeft($pel_no[$i],14).							"             ".$pel_nama[$i].chr(10);
								$stringCetak .= str_repeat(' ',10).printLeft(substr($pel_nama[$i],0,14),14).			"             ".$pel_alamat[$i].chr(10);
								$stringCetak .= str_repeat(' ',10).printLeft(substr($pel_alamat[$i],0,14),14).			"             ".printLeft($kopel,25).$rek_gol[$i].chr(10);
								$stringCetak .= str_repeat(' ',10).printLeft($rek_gol[$i]."/".$dkd_kd[$i],14).			"             ".printLeft($tgl_baca[$i],25).$dkd_kd[$i].chr(10);
								$stringCetak .= str_repeat(' ',10).printLeft(substr($kopel,0,14),14).chr(10);
								$stringCetak .= printRight(number_format($rek_uangair[$i])."  ",24).					" ".printRight(number_format($rek_stankini[$i])." ",7).printRight(number_format($rek_beban[$i]),48).chr(10);
								$stringCetak .= str_repeat(' ',24).														" ".printRight($o_awalA,12).printRight($o_akhirA,5).printRight($o_hargaA,8).printRight(number_format($o_uangairA),11).printRight(number_format($rek_angsuran[$i]),19).chr(10);
								$stringCetak .= printRight(number_format($beban_tetap[$i])."  ",24).					" ".printRight(number_format($rek_stanlalu[$i])." ",7).printRight($o_awalB,5).printRight($o_akhirB,5).printRight($o_hargaB,8).printRight(number_format($o_uangairB),11).printRight(number_format($rek_denda[$i]),19).chr(10);
								$stringCetak .= printRight(number_format($rek_angsuran[$i])."  ",24).					" ".printRight($o_awalC,12).printRight($o_akhirC,5).printRight($o_hargaC,8).printRight(number_format($o_uangairC),11).chr(10);
								$stringCetak .= printRight(number_format($rek_denda[$i])."  ",24).						" ".printRight(number_format($rek_stankini[$i]-$rek_stanlalu[$i])." ",7).printRight(number_format($rek_uangair[$i]),29).printRight(number_format($rek_bayar[$i]),19).chr(10);
								$stringCetak .= printRight(number_format($rek_bayar[$i])."  ",24).chr(10);

								// line spacing x/n
								$stringCetak .= chr(27).chr(65).chr(8);
								$stringCetak .= chr(10);

								// line spacing x/n
								$stringCetak .= chr(27).chr(65).chr(12);
								$stringCetak .= str_repeat(' ',8).printLeft($tgl_sekarang,16).							" ".strtoupper(substr((n2c($rek_bayar[$i],"Rupiah")),0,34)).chr(10);
								$stringCetak .= str_repeat(' ',24).														" ".strtoupper(substr((n2c($rek_bayar[$i],"Rupiah")),34,34)).chr(10);
								$stringCetak .= printRight($byr_no."/".$loket."/"._USER,24).							" ".$byr_no."/".$loket."/"._USER.chr(10).chr(10).chr(10);

								$que	= "UPDATE tm_rekening SET rek_denda=$rek_denda[$i],rek_byr_sts=1 WHERE rek_nomor=$rek_nomor[$i] AND rek_sts=1 AND rek_byr_sts=0 AND (rek_total+$rek_denda[$i])=$rek_bayar[$i]";
								$st 	= $db->exec($que);
								if($st>0){
									errorLog::logDB(array($que));
								}
								else{
									$erno 	= true;
									$mess	= "Gagal update tabel rekening";
									$i		= count($pilih) + 1;
								}
								$que	= "INSERT INTO tm_pembayaran(byr_no,byr_tgl,byr_serial,rek_nomor,kar_id,lok_ip,byr_loket,byr_total,byr_cetak,byr_upd_sts,byr_sts) VALUES($byr_no,NOW(),'$noresi',$rek_nomor[$i],'$kar_id','$ipClient','$loket',$rek_bayar[$i],0,NOW(),1)";
								$st 	= $db->exec($que);
								if($st>0){
									errorLog::logDB(array($que));
									$noresi++;
								}
								else{
									$erno 	= true;
									$mess	= "Gagal insert tabel pembayaran";
									$i		= count($pilih) + 1;
								}
								$j++;
							}
							catch (PDOException $err){
								$erno 	= true;
								$i		= count($pilih) + 1;
								$mess 	= "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
								$klas	= "error";
								errorLog::errorDB(array($que));
							}
							$l++;
						}
					}
					$stringCetak .= chr(12);

					if(!$erno){
						try{
							$que	= "INSERT INTO tm_bayar_rek(byr_no,byr_tgl,kar_id,byr_loket,byr_total,byr_dibayar,byr_kembali,byr_sts) VALUES($byr_no,NOW(),'$kar_id','$loket',$bayar,$dibayar,$kembalian,1)";
							$st 	= $db->exec($que);
							if($st>0){
								errorLog::logDB(array($que));
							}
							else{
								$erno 	= true;
								$mess	= "Gagal insert tabel bayar rek";
								$i		= count($pilih) + 1;
							}
							$que	= "UPDATE system_parameter SET sys_value1='$noresi' WHERE sys_param='RESI' AND sys_value='$kar_id'";
							$st 	= $db->exec($que);
							if($st>0){
								errorLog::logDB(array($que));
							}
							else{
								$erno 	= true;
								$mess	= "Gagal update nomer resi";
								$i		= count($pilih) + 1;
							}
						}
						catch (PDOException $err){
							$erno 	= true;
							errorLog::errorDB(array($que));
							$mess 	= "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
						}
					}

					try {
						$stringFile	  = $byr_no.".txt";
						$openFile 	  = fopen("_data/".$stringFile, 'w');
						fwrite($openFile, $stringCetak);
						fclose($openFile);
						$client->cetak(base64_encode($stringCetak),$stringFile);
					}
					catch (Exception $err) {
						$erno 	= true;
						$mess	= "Proses cetak resi bayar gagal dilakukan";
						errorLog::errorDB(array($mess));
						$mess	= $e->getMessage();
						$klas	= "error";
					}
					
					// commit status proses transaksi
					if($erno){
						$db->rollBack();
						$j=0;
					}
					else{
						//$db->rollBack();
						$db->commit();
					}

					if($j>0){
						$mess	= "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
						$klas 	= "success";
					}
					else{
						$mess	= "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
						$klas 	= "error";
					}
				}
				else{
					$mess	= "Uang yang diterima tidak mencukupi. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
					$klas 	= "error";
				}
			}
			break;
		default:
			$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
			$klas = "error";
	}
	errorLog::logMess(array($mess));
	echo "<input type=\"hidden\" id=\"$errorId\" value=\"$mess\"/>";
	unset($db);
?>