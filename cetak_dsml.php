<?php
	if($erno) die();
	$formId = getToken();

	$que0 	= "SELECT a.* FROM v_dsml a WHERE a.dkd_kd='$dkd_kd' ORDER BY a.pel_no";
	try{
		$res0 = mysql_query($que0,$link);
		while($row0 = mysql_fetch_array($res0)){
			$data[] = $row0;
		}
	}
	catch (Exception $e){
		errorLog::errorDB(array($que0));
		$mess = $e->getMessage();
	}
	// line untuk ff continous paper
	$stringCetak  = chr(27).chr(67).chr(1);
	// enable paper out sensor
	$stringCetak .= chr(27).chr(57);
	// draft mode
	$stringCetak .= chr(27).chr(120).chr(48);
    // mode 10 cpi
    $stringCetak .= chr(27).chr(80);
	// line spacing x/72
	$stringCetak .= chr(27).chr(65).chr(24);

	$halA	= 25;
	$nomer 	= 0;
	for($j=0;$j<count($data);$j++){
		$nomer++;
		/** getParam 
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		$nilai	= $data[$j];
		$konci	= array_keys($nilai);
		for($i=0;$i<count($konci);$i++){
			$$konci[$i]	= $nilai[$konci[$i]];
		}
		/* getParam **/
		if($nomer==1 or ($j%$halA)==0){
			$stringCetak .= "PERUSAHAAN DAERAH AIR MINUM TIRTA SAKTI KAB. KERINCI".chr(10);
			$stringCetak .= "CABANG / UNIT : ".printLeft($_SESSION['kp_ket'],24).	"HALAMAN   : ".printRight(ceil($nomer/$halA),2).printRight("Bulan : ".$bulan[date('n')]." ".date('Y'),26).chr(10);
			$stringCetak .= "PENCATAT      : ".printLeft($dkd_pembaca,24).			"TGL CATAT : ".$dkd_tcatat.chr(10);
			$stringCetak .= "WILAYAH       : ".printLeft(substr($dkd_kd,-3),24).	"JALAN     : ".$dkd_jalan.chr(10);
			$stringCetak .= str_repeat('-',80).chr(10);
			$stringCetak .= printLeft("NO.",3);
			$stringCetak .= printLeft(" NAMA",24);
			$stringCetak .= printLeft(" ALAMAT",20);
			$stringCetak .= printLeft("GOL.",4);
			$stringCetak .= printRight("NO.SL",10);
			$stringCetak .= printCenter("KINI",7);
			$stringCetak .= printRight("KETERANGAN",11).chr(10);
			$stringCetak .= str_repeat('-',80).chr(10);
		}
		
		$pel_no		= $pel_no;
		$pel_nama	= substr(" ".$pel_nama,0,24);
		$pel_alamat	= substr(" ".$pel_alamat,0,20);
		$stringCetak .= printRight($nomer,3);
		$stringCetak .= printLeft($pel_nama,24);
		$stringCetak .= printLeft($pel_alamat,20);
		$stringCetak .= printLeft($gol_kode,4);
		$stringCetak .= printRight($pel_no,10);
		if($sm_sts==2){
			$stringCetak .= printRight("______",7);
			$stringCetak .= printRight(" ",12);
		}
		else{
			$stringCetak .= printRight($sm_kini,7);
			$stringCetak .= " ".printRight(($sm_kini-$sm_lalu),11);
		}
		$stringCetak .= chr(10);
		
		if($nomer%$halA==0){
			$stringCetak .= chr(10);
		}
	}
	
	if(($nomer%$halA)>14 or ($nomer%$halA)==0){
		if(($nomer%$halA)>0){
			for($m=0;$m<(1+$halA-($nomer%$halA));$m++){
				$stringCetak .= chr(10);
			}
		}
		$stringCetak .= "PERUSAHAAN DAERAH AIR MINUM TIRTA SAKTI KAB. KERINCI".chr(10);
		$stringCetak .= "CABANG / UNIT : ".printLeft($_SESSION['kp_ket'],24).	"HALAMAN   : ".printRight((ceil($nomer/$halA)+1),2).printRight("Bulan : ".$bulan[date('n')]." ".date('Y'),26).chr(10);
		$stringCetak .= "PENCATAT      : ".printLeft($dkd_pembaca,24).			"TGL CATAT : ".$dkd_tcatat.chr(10);
		$stringCetak .= "WILAYAH       : ".printLeft(substr($dkd_kd,-3),24).	"JALAN     : ".$dkd_jalan.chr(10);
		$stringCetak .= str_repeat('-',80).chr(10).chr(10).chr(10);
		$stringCetak .= printCenter("Diketahui Oleh :",29).printCenter("Diperiksa Oleh :",30).printCenter("Petugas :",24).chr(10);
		$stringCetak .= printCenter("Kepala Cabang",29).printCenter("Kasubag Hublang / Korektor",30).printCenter("Pembaca Meter",24).chr(10).chr(10).chr(10);
		$stringCetak .= printCenter("(________________)",29).printCenter("(________________)",30).printCenter("(________________)",24).chr(10);
		for($m=0;$m<16;$m++){
			$stringCetak .= chr(10);
		}
	}
	else{
		$stringCetak .= str_repeat('-',80).chr(10).chr(10);
		$stringCetak .= printCenter("Diketahui Oleh :",29).printCenter("Diperiksa Oleh :",30).printCenter("Petugas :",24).chr(10);
		$stringCetak .= printCenter("Kepala Cabang",29).printCenter("Kasubag Hublang / Korektor",30).printCenter("Pembaca Meter",24).chr(10).chr(10).chr(10);
		$stringCetak .= printCenter("(________________)",29).printCenter("(________________)",30).printCenter("(________________)",24).chr(10);
		for($m=0;$m<($halA-($nomer%$halA)-11);$m++){
			$stringCetak .= chr(10);
		}
	}

	$stringCetak .= chr(12);
	$stringFile	  = "_data/"._TOKN.".txt";
	$openFile 	  = fopen($stringFile, 'w');
	fwrite($openFile, $stringCetak);
	fclose($openFile);
		
	try{
		$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
		$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
		$cetak 		= true;
	}
	catch (Exception $e){
		echo $e->getMessage();
		$cetak 		= false;
	}
	$stringFile	  = _TOKN.".txt";
	$client->cetak(base64_encode($stringCetak),$stringFile);
	echo "DSML telah dicetak";
?>