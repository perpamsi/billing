<?php
	if($erno) die();
	$kar_id = _USER;
	
	/** koneksi ke database */
	$db		= false;
	try {
		$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $err){
		$mess = $err->getTrace();
		errorLog::errorDB(array($mess[0]['args'][0]));
		$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan";
		$klas = "error";
	}
	
	if($db){
		try {
			$db->beginTransaction();
			$que	= "UPDATE tm_reduksi SET rd_sts=0 WHERE rek_nomor=$rek_nomor";
			$st 	= $db->exec($que);
			$que	= "INSERT INTO tm_reduksi(appl_tokn,pel_no,rek_nomor,rd_tgl,rd_stanlalu,rd_stankini,rd_uangair_awal,rd_nilai,rd_uangair_akhir,rd_rek_no_baru,gol_kode,rd_kode,rd_sts,kar_id) VALUES("._TOKN.",'$pel_no',$rek_nomor,NOW(),$rek_stanlalu,$rek_stankini,$rek_uangair,$reduksi,$rek_reduksiuangair,1,$rek_gol,5,1,'$kar_id')";
			$st 	= $db->exec($que);
			if($st>0){
				$db->commit();
				//$db->rollBack();
				errorLog::logDB(array($que));
				$mess = "Proses Reduksi SL:$pel_no telah berhasil dilakukan.";
				$klas = "success";
			}
		}
		catch (PDOException $err){
			errorLog::errorDB(array($que));
			$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses simpan Reduksi SL: $pel_no tidak bisa dilakukan.";
			//$mess = $que;
			$klas = "error";
		}
	}
	
	errorLog::logMess(array($mess));
	//echo "<div class=\"$klas\">$mess</div>";
	echo $mess;
	echo "<input type=\"button\" value=\"Kembali\" onclick=\"buka('kembali')\" />";
	unset($db);
?>