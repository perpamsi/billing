<?php
	if($erno) die();
	$kar_id = _USER;
	$byr_no	= _TOKN;
	$kopel	= $_SESSION['kp_ket'];
	
	/** koneksi ke database */
	$db		= false;
	try {
		$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		unset($mess);
	}
	catch (PDOException $err){
		$mess = $err->getTrace();
		errorLog::errorDB(array($mess[0]['args'][0]));
		$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
		$klas = "error";
	}
	switch($proses){
		case "setResi":
			$db->beginTransaction();
			if($db){
				try {
					$que	= "INSERT INTO system_parameter(sys_param,sys_value,sys_value1,sys_value2) VALUES('RESI','$kar_id','$noresi','-') ON DUPLICATE KEY UPDATE sys_value1='$noresi'";
					$st 	= $db->exec($que);
					if($st>0){
						$db->commit();
						errorLog::logDB(array($que));
						$mess = "Set resi: $noresi telah dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
					}
					else{
						$db->rollBack();
						$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
					}
				}
				catch (PDOException $err){
					$db->rollBack();
					$mess = $err->getTrace();
					errorLog::errorDB(array($mess[0]['args'][0]));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses set resi tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
				}
			}
			break;
		case "bayar":
			try{
				$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
				$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
				$cetak 		= true;
			}
			catch (Exception $e){
				$mess		= "Perangkat pencetak belum tersedia";
				errorLog::errorDB(array($mess));
				$mess		= $e->getMessage();
				$klas		= "error";
				$cetak 		= false;
			}
			
			if($cetak){
				$db->beginTransaction();
				$erno	= false;
				if($kembalian>=0){
					try {
						$kopel		  = "CABANG ".$_SESSION['kp_ket'];
						if($na_kode==1){
							$catatan  = "CATATAN   : TAGIHAN REKENING AIR MULAI DIBAYAR BULAN ".strtoupper($bulan[date('n')])." ".date('Y').". DAN HARGA SUDAH TERMASUK PAJAK.".chr(10);
						}
						else if($na_kode<5){
							$catatan  = "CATATAN   : TAGIHAN REKENING AIR MULAI DIBAYAR BULAN ".strtoupper($bulan[date('n')])." ".date('Y').chr(10);
						}
						else{
							$catatan  = chr(10);
						}
						
						$rayon = $dkd_kd."/".$pel_alamat;
						if($pel_no=='043001'){
							$rayon		= '';
							$dkd_kd 	= '';
							$pel_alamat	= '';
							$gol_kode	= '';
						}
						
						// line untuk ff continous paper
						$stringCetak  = chr(27).chr(67).chr(1);
						// enable paper out sensor
						$stringCetak .= chr(27).chr(57);
						// draft mode
						$stringCetak .= chr(27).chr(120).chr(48);
						// line spacing x/72
						$stringCetak .= chr(27).chr(65).chr(12);

						$stringCetak .= printLeft("PERUSAHAAN DAERAH AIR MINUM TIRTA SAKTI",50).chr(10);
						$stringCetak .= printLeft(strtoupper($kopel),50).printRight("KWITANSI",30).chr(10);
						$stringCetak .= printLeft("KABUPATEN KERINCI",50).printRight("NO. ".$noresi,30).chr(10);
						$stringCetak .= str_repeat("-",80).chr(10);
						$stringCetak .= printLeft("SUDAH DITERIMA DARI",20).": ".str_repeat(". ",10).chr(10);
						$stringCetak .= printLeft("NOMOR SAMBUNG",20).": ".$pel_no.chr(10);
						$stringCetak .= printLeft("RAYON/ALAMAT",20).": ".$rayon.chr(10);
						$stringCetak .= printLeft("GOLONGAN",20).": ".$gol_kode.chr(10);
						$stringCetak .= printLeft("JUMLAH",20).": ".number_format($bayar,0).chr(10);
						$stringCetak .= printLeft("UNTUK PEMBAYARAN",20).": ".strtoupper($na_ket).chr(10);
						$stringCetak .= str_repeat("-",80).chr(10);
						$stringCetak .= printLeft("TERBILANG : ".strtoupper(n2c($bayar,"Rupiah"))).chr(10);
						$stringCetak .= $catatan.chr(10);
						$stringCetak .= str_repeat(" ",50).printCenter("KERINCI, ".date('d')." ".strtoupper($bulan[date('n')])." ".date('Y'),30).chr(10);
						$stringCetak .= str_repeat(" ",50).printCenter("KEPALA CABANG",30).chr(10).chr(10).chr(10);
						$stringCetak .= str_repeat(" ",50).printCenter("(______________)",30).chr(10);
						$stringCetak .= str_repeat(" ",50).printCenter("NIP :           ",30).chr(10);

						$que	= "INSERT INTO tm_pembayaran_non_air(byr_no,byr_tgl,byr_serial,pel_no,kar_id,lok_ip,na_kode,byr_total,byr_cetak,byr_upd_sts,byr_sts) VALUES($byr_no,NOW(),'$noresi','$pel_no','$kar_id','$ipClient','$na_kode',$bayar,0,NOW(),1)";
						$st 	= $db->exec($que);
						if($st>0){
							errorLog::logDB(array($que));
							$noresi++;
						}
						else{
							$erno 	= true;
							$mess	= "Gagal insert tabel pembayaran";
							$i		= count($pilih) + 1;
						}

						$que	= "UPDATE system_parameter SET sys_value1='$noresi' WHERE sys_param='RESI' AND sys_value='$kar_id'";
						$st 	= $db->exec($que);
						if($st>0){
							errorLog::logDB(array($que));
						}
						else{
							$erno 	= true;
							$mess	= "Gagal update nomer resi";
						}
					}
					catch (PDOException $err){
						$erno 	= true;
						$mess 	= "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>Esc</b> untuk menutup pesan ini";
						$klas	= "error";
						errorLog::errorDB(array($que));
					}

					$stringCetak .= chr(12);

					if(!$erno){
						try {
							$stringFile	  = $byr_no.".txt";
							$openFile 	  = fopen("_data/".$stringFile, 'w');
							fwrite($openFile, $stringCetak);
							fclose($openFile);
							$client->cetak(base64_encode($stringCetak),$stringFile);
						}
						catch (Exception $err) {
							$erno 	= true;
							$mess	= "Proses cetak resi bayar gagal dilakukan";
							errorLog::errorDB(array($mess));
							$mess	= $e->getMessage();
							$klas	= "error";
						}
					}
					
					// commit status proses transaksi
					$j = 1;
					if($erno){
						$db->rollBack();
						$j = 0;
					}
					else{
						//$db->rollBack();
						$db->commit();
					}

					if($j>0){
						$mess	= "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
						$klas 	= "success";
					}
					else{
						$mess	= "$j transaksi telah dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
						$klas 	= "error";
					}
				}
				else{
					$mess	= "Uang yang diterima tidak mencukupi. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
					$klas 	= "error";
				}
			}
			break;
		default:
			$mess = "Mungkin telah terjadi kesalahan pada prosedur aplikasi, sehingga proses pembayaran tidak bisa dilakukan. Tekan tombol <b>B</b> untuk kembali ke halaman semula";
			$klas = "error";
	}
	errorLog::logMess(array($mess));
	echo "<input type=\"hidden\" id=\"$errorId\" value=\"$mess\"/>";
	unset($db);
?>