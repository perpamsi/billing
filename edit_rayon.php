<?php
	if($erno) die();
	/** koneksi ke database */
	$db		= false;
	try {
		$db 	= new PDO($PSPDO[0],$PSPDO[1],$PSPDO[2]);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $err){
		$mess = $err->getTrace();
		errorLog::errorDB(array($mess[0]['args'][0]));
		$mess = "Mungkin telah terjadi kesalahan pada database server, sehingga koneksi tidak bisa dilakukan";
		$klas = "error";
	}
	
	switch($proses){
		case "addRayon":
			if($db){
				try {
					$db->beginTransaction();
					$que	= "INSERT INTO tr_dkd(dkd_kd,dkd_no,dkd_jalan,dkd_loket,kar_id,dkd_tcatat) VALUES(CONCAT('"._KOTA."','$dkd_rayon',REPEAT(0,2-LENGTH('$dkd_no')),'$dkd_no'),'$dkd_no','".trim($dkd_jalan)."','$dkd_loket','$dkd_pembaca',$dkd_tcatat)";
					$st 	= $db->exec($que);
					$db->commit();
					errorLog::logDB(array($que));
					if($st>0){
						$dkd_kd	= _KOTA.$dkd_rayon.str_repeat(0,2-strlen($dkd_no)).$dkd_no;
						$mess 	= "Informasi rayon:$dkd_kd telah ditambahkan.";
						$klas 	= "success";
					}
				}
				catch (PDOException $err){
					$db->rollBack();
					$mess = $err->getTrace();
					errorLog::errorDB(array($mess[0]['args'][0]));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga proses tambah rayon:$dkd_rayon-$dkd_no tidak bisa dilakukan.";
					$klas = "error";
				}
			}
			break;
		case "editRayon":
			if($db){
				$kopel = explode("_",$kopel);
				try {
					$db->beginTransaction();
					$que	= "UPDATE tr_dkd SET dkd_no='$dkd_no',dkd_rayon='$dkd_rayon',kar_id='$kar_id',dkd_tcatat='$dkd_tcatat',dkd_jalan='".trim($dkd_jalan)."',dkd_kd=CONCAT('".$kopel[0]."',REPEAT(0,2-LENGTH('$dkd_rayon')),'$dkd_rayon',REPEAT(0,2-LENGTH('$dkd_no')),'$dkd_no') WHERE dkd_kd='$dkd_kd'";
					$st 	= $db->exec($que);
					$db->commit();
					errorLog::logDB(array($que));
					if($st>0){
						$mess = "Informasi rayon: $dkd_kd telah diperbaharui";
						$klas = "success";
					}
				}
				catch (PDOException $err){
					$db->rollBack();
					$mess = $err->getTrace();
					errorLog::errorDB(array($mess[0]['args'][0]));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga proses edit rayon:$dkd_kd tidak bisa dilakukan.";
					$klas = "error";
				}
			}
			break;
		case "deleteRayon":
			if($db){
				try {
					$db->beginTransaction();
					$que	= "DELETE FROM tr_dkd WHERE dkd_kd='$dkd_kd'";
					$st 	= $db->exec($que);
					$db->commit();
					errorLog::logDB(array($que));
					if($st>0){
						$mess = "Rayon: $dkd_kd telah dihapus dari database.";
						$klas = "success";
					}
				}
				catch (PDOException $err){
					$db->rollBack();
					$mess = $err->getTrace();
					errorLog::errorDB(array($mess[0]['args'][0]));
					$mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga proses delete rayon:$dkd_kd tidak bisa dilakukan.";
					$klas = "error";
				}
			}
			break;
		default:
			$mess = "Mungkin telah terjadi kesalahan pada prosedur manual, sehingga tidak ada proses yang bisa dijalankan.";
			$klas = "notice";
	}
	errorLog::logMess(array($mess));
	echo "<div class=\"$klas left\">$mess</div>";
?>