<?php
	if($erno) die();
	$formId 	= getToken();
	$kp_kode	= $_SESSION['Kota_c'];
	$kp_ket		= $_SESSION['kp_ket'];
	$erno		= false;
	
	$que0 = "SELECT a.* FROM v_dsr a JOIN tm_kolektif b ON(b.pel_no=a.pel_no) WHERE b.kel_kode='".$kel_kode."' ORDER BY a.pel_no,a.rek_thn ASC,a.rek_bln ASC";
	try{
		$res0 = mysql_query($que0,$link);
		while($row0 = mysql_fetch_array($res0)){
			$data[$row0['dkd_kd']][] = $row0;
		}
		$mess = "DRD Kolektif : ".$kel_kode." telah dicetak";
	}
	catch (Exception $e){
		errorLog::errorDB(array($que0));
		$mess = "Terjadi kesalahan pada sistem aplikasi";
		$erno = true;
	}
	// line untuk ff continous paper
	$stringCetak  = chr(27).chr(67).chr(1);
	// enable paper out sensor
	$stringCetak .= chr(27).chr(57);
	// draft mode
	$stringCetak .= chr(27).chr(120).chr(48);
	// mode 12 cpi
	$stringCetak .= chr(27).chr(77);
	// line spacing x/72
	$stringCetak .= chr(27).chr(65).chr(12);

	$halaman	= 1;
	if(count($data)>0){
		$level1_val 	= $data;
		$level1_key 	= array_keys($level1_val);
		/* order by level 1 pelanggan */
		for($i=0;$i<count($level1_val);$i++){
			// resume bisa ditambahkan di sini
			$rayon		  = $level1_val[$level1_key[$i]][0][11];
				
			$level2_val		= $level1_val[$level1_key[$i]];
			$level2_key		= array_keys($level2_val);
			/* order by level 2 rincian tunggakan */
			for($k=0;$k<count($level2_val);$k++){
				if($k==0){
					if($k>0){
						$stringCetak .= chr(10).chr(10).chr(10).chr(10).chr(10);
					}
					$stringCetak .= printCenter("PERUSAHAAN DAERAH AIR MINUM TIRTA SAKTI",81).printLeft("TGL. ".date('d/m/Y'),15).chr(10);
					$stringCetak .= printCenter("DAFTAR REKENING DITAGIH (DRD-AIR) UNTUK KELOMPOK : ".$kolektor,81).printLeft("HALAMAN ".$halaman,15).chr(10);
					$stringCetak .= printLeft("Cabang / Kode : ".$kp_ket,50).printRight($kel_kode,46).chr(10);
					$stringCetak .= str_repeat('=',96).chr(10);
					$stringCetak .= printCenter("NO",4);
					$stringCetak .= printLeft("NOSL",7);
					$stringCetak .= printLeft("NAMA",18);
					$stringCetak .= printRight("GOL",4);
					$stringCetak .= printRight("LALU",8);
					$stringCetak .= printRight("KINI",8);
					$stringCetak .= printRight("BULAN",11);
					$stringCetak .= printRight("HARGA AIR",13);
					$stringCetak .= printRight("BEBAN",8);
					$stringCetak .= printRight("TOTAL",14).chr(10);
					$stringCetak .= str_repeat('=',96).chr(10);
					$halaman++;
				}
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $level2_val[$level2_key[$k]];
				$konci	= array_keys($nilai);
				for($l=0;$l<count($konci);$l++){
					$$konci[$l]	= $nilai[$konci[$l]];
				}
				/* getParam **/
				$stringCetak .= printRight(($k+1),3);
				$stringCetak .= printLeft(" ".$pel_no,7);
				$stringCetak .= printLeft(substr(" ".$pel_nama,0,19),19);
				$stringCetak .= printRight($rek_gol,4);
				$stringCetak .= printRight(number_format($rek_stanlalu),8);
				$stringCetak .= printRight(number_format($rek_stankini),8);
				$stringCetak .= printRight($tagihan,11);
				$stringCetak .= printRight(number_format($rek_uangair),13);
				$stringCetak .= printRight(number_format($beban_tetap),8);
				$stringCetak .= printRight(number_format($rek_total),15).chr(10);
				
				$l0_pakai[$i][]		= $rek_pakai;
				$l0_uangair[$i][]	= $rek_uangair;
				$l0_beban[$i][]		= $beban_tetap;
				$l0_total[$i][]		= $rek_total;
			}
			$stringCetak .= str_repeat('=',96).chr(10);
			$stringCetak .= printRight("Jumlah Akhir :",81);
			$stringCetak .= printRight(number_format(array_sum($l0_total[$i])),15).chr(10);
		}
	}
	for($m=0;$m<(56-($k%56));$m++){
		$stringCetak .= chr(10);
	}
	$stringCetak .= chr(12);
	$stringFile	  = "_data/"._TOKN.".txt";
	$openFile 	  = fopen($stringFile, 'w');
	fwrite($openFile, $stringCetak);
	fclose($openFile);
		
	try{
		$wsdl_url 	= "http://"._PRIN."/printClient/printServer.wsdl";
		$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
		$cetak 		= true;
	}
	catch (Exception $e){
		echo $e->getMessage();
		$cetak 		= false;
	}
	$stringFile	  = _TOKN.".txt";
	$client->cetak(base64_encode($stringCetak),$stringFile);
	echo "<div class=\"success\">".$mess."</div>";
?>